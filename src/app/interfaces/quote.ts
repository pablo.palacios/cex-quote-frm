export interface Quote {
  stand_id: number;
  service_product_id: number;
  quoted_by: string;
  quote_email: string;
  quote_phone: string;
  service_product_no_of_items: string;
  quote_country: number;
  quote_province: number;
  quote_notes: string;
  quote_price: number;
  quote_c1: number;
  askRelated?: boolean;
}
