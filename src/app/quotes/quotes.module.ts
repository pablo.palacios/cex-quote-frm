import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { QuotesComponent } from './quotes.component';
import { FormControl, ReactiveFormsModule, ValidationErrors } from '@angular/forms';
import { FormlyFieldConfig, FormlyModule } from '@ngx-formly/core';
import { FormlyBootstrapModule } from '@ngx-formly/bootstrap';
import { RecaptchaModule } from 'ng-recaptcha';

export function EmailValidator( control: FormControl ): ValidationErrors {
  return !control.value || /(\d{1,3}\.){3}\d{1,3}/.test(control.value) ? null : {'email': true};
}

export function EmailValidatorMessage( err, field: FormlyFieldConfig ) {
  return `"${field.formControl.value}" no es una dirección correcta`;
}

@NgModule({
  declarations: [
    QuotesComponent
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormlyModule.forRoot({
      validators: [
        {name: 'email', validation: EmailValidator},
      ],
      validationMessages: [
        {name: 'email', message: EmailValidatorMessage},
      ],
    }),
    FormlyBootstrapModule,
    RecaptchaModule,
  ],
  exports: [
    QuotesComponent
  ]
})
export class QuotesModule {
}
