import { Injectable } from '@angular/core';

import { environment } from '../../environments/environment';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class QuotesService {

  constructor(
    private http: HttpClient
  ) {
  }

  getSettings() {
    return this.http.get(environment.apiUrl + '/api/settings');
  }

  getCountry() {
    return this.http.get(environment.apiUrl + '/api/get-country');
  }

  getProvinces( id ) {
    return this.http.get(`${environment.apiUrl}/api/get-provinces?id=${id}`);
  }

  getProduct( id ) {
    return this.http.get(`${environment.apiUrl}/api/get-product?id=${id}`);
  }

  saveQuote( model ) {
    return this.http.post(environment.apiUrl + '/api/save-quote', model);
  }

}
