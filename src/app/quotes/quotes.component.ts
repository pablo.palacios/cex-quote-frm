import {Component, OnInit} from '@angular/core';
import {FormGroup} from '@angular/forms';
import {FormlyFieldConfig} from '@ngx-formly/core';
import {Quote} from '../interfaces/quote';
import {QuotesService} from './quotes.service';
import {ActivatedRoute} from '@angular/router';
import {environment} from '../../environments/environment';

@Component({
  selector: 'cex-quotes',
  templateUrl: './quotes.component.html',
  styleUrls: ['./quotes.component.css'],
})
export class QuotesComponent implements OnInit {

  public form = new FormGroup({});
  public model: any | Quote = {};
  public product: any = {};
  public category: any = {};
  fields: FormlyFieldConfig[] = [];
  public selectedProduct = true;
  public searchingProduct = true;
  public isCaptchaValid = false;
  isSending = false;
  private countryModel = [];
  private provinceModel = [];
  private productId: any;
  private productImg: any;
  private defType = '';
  private category2: any = {};
  private category3: any = {};
  private stand: any = {};
  private standImg: string;
  public labelQuoteSingular: string;
  public labelQuotePlural: string;
  public labelQuoteAction: string;

  constructor(
    private quotesService: QuotesService,
    private route: ActivatedRoute
  ) {
    this.route.paramMap.subscribe(params => {
      this.productId = params.get('id');
    });
  }

  ngOnInit() {
    let route$ = this.route.paramMap.subscribe(params => {
        this.productId = params.get('id');
        let product$ = this.quotesService
          .getProduct(this.productId)
          .subscribe((productData: any) => {
            this.searchingProduct = false;
            this.product = productData['product'];
            this.category = productData['category'];
            this.category2 = productData['category_2'];
            this.category3 = productData['category_3'];
            this.stand = productData['stand'];

            if (this.product.image.includes('uploads')) {
              this.productImg = environment.apiUrl + this.product.image;
            } else {
              this.productImg = `${environment.apiUrl}/images/upload/${this.stand.id}/card/${this.product.image}`;
            }

            this.standImg = `${environment.apiUrl}/images/upload/${this.stand.id}/card/${this.stand.img}`;
            this.defType = this.model.item_type;


          });
      },
      (error: any) => {
      },
      () => route$.unsubscribe());

    let settings$ = this.quotesService
      .getSettings()
      .subscribe(
        (settings: any) => {
          this.model.quote_country = settings.main_country_id;
          this.labelQuoteSingular = settings.label_quote_singular;
          this.labelQuotePlural = settings.label_quote_plural;
          this.labelQuoteAction = settings.label_quote_action;
          let country$ = this.quotesService
            .getCountry()
            .subscribe(
              (countryData: any) => {
                this.countryModel.push(countryData);
                let provinces$ = this.quotesService
                  .getProvinces(settings.main_country_id)
                  .subscribe(
                    (provincesData: any) => {
                      this.provinceModel = provincesData;
                      this.provinceModel.push({id: '999', name: '[ OTRO ]'});
                      if (this.getLocalModel() !== null) {
                        this.model = this.getLocalModel();
                        this.model.askRelated = true;
                        this.model.service_product_no_of_items = 1;
                        this.model.quote_notes = null;

                      } else {
                        this.model.askRelated = true;
                        this.model.quote_province = '';
                        this.model.service_product_no_of_items = 1;
                      }

                      this.model.stand_id = this.product.stand_id;
                      this.model.service_product_id = this.product.id;
                      this.model.item_type = this.product.item_type;
                      this.model.quote_price = this.product.price;
                      this.model.quote_c1 = this.category.id;
                      this.model.quote_c2 = this.category2.id;
                      this.model.quote_c3 = this.category3.id;

                      this.fields = this.setFields();
                    },
                    (error: any) => {
                    },
                    () => provinces$.unsubscribe()
                  );
              },
              (error: any) => {
              },
              () => country$.unsubscribe()
            );
        },
        (error: any) => {
        },
        () => settings$.unsubscribe()
      );
  }

  resolved(captchaResponse: string) {
    if (captchaResponse !== null) {
      this.isCaptchaValid = true;
    }
  }

  submit(model) {
    this.setLocalModel(model);
    if (this.form.valid || this.isCaptchaValid) {
      this.isSending = true;
      this.quotesService.saveQuote(model)
        .subscribe((saveRsp: any) => {
            if (saveRsp.code === '200') {
              window.top.location.href = `${environment.apiUrl}/contenidos/gracias`;
            }
          },
          (e) => console.error(e),
          () => console.info('final'));
    }
  }

  private setLocalModel(model) {
    localStorage.setItem('model', JSON.stringify(model));
  }

  private getLocalModel() {
    return JSON.parse(localStorage.getItem('model'));
  }

  private setFields() {
    return [
      {
        fieldGroupClassName: 'row',
        fieldGroup: [
          {
            className: 'col-6',
            key: 'quote_country',
            type: 'select',
            templateOptions: {
              placeholder: 'País',
              required: true,
              options: this.countryModel,
              valueProp: 'id',
              labelProp: 'name',
              readonly: true,
              disabled: true,
              attributes: {
                class: 'form-control form-control-sm'
              },
            },
            validation: {
              messages: {
                required: (error, field: FormlyFieldConfig) => `Debe seleccionar un país`,
              },
            },
          },
          {
            className: 'col-6',
            key: 'quote_province',
            type: 'select',
            templateOptions: {
              placeholder: 'Provincia...',
              required: true,
              options: this.provinceModel,
              valueProp: 'id',
              labelProp: 'name',
              attributes: {
                class: 'form-control form-control-sm'
              },
            },
            validation: {
              messages: {
                required: (error, field: FormlyFieldConfig) => `Debe seleccionar una provincia`,
              },
            },
          },
          {
            className: 'col-12',
            type: 'input',
            key: 'quoted_by',
            templateOptions: {
              placeholder: 'Su Nombre',
              required: true,
              attributes: {
                class: 'form-control form-control-sm'
              },
            },
            validation: {
              messages: {
                required: (error, field: FormlyFieldConfig) => `El nombre es requerido`,
              },
            },
          },
          {
            className: 'col-6',
            type: 'input',
            key: 'quote_email',
            templateOptions: {
              placeholder: 'Su Correo electrónico',
              required: true,
              attributes: {
                class: 'form-control form-control-sm'
              },
            },
            validators: {
              quote_email: {
                expression: (c) => !c.value || /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(c.value),
                message: (error, field: FormlyFieldConfig) => `"${field.formControl.value}" no es una dirección de email`,
              },
            },
          },
          {
            className: 'col-6',
            type: 'input',
            key: 'quote_phone',
            templateOptions: {
              placeholder: 'Su teléfono',
              description: 'Ej. 02-222-2222 o 099-999-9999',
              required: true,
              attributes: {
                class: 'form-control form-control-sm'
              },
            },
          },
        ],
      },
      {
        fieldGroupClassName: 'row',
        fieldGroup: [

          {
            className: 'col-6',
            type: 'input',
            key: 'service_product_no_of_items',
            templateOptions: {
              type: 'number',
              placeholder: 'Cuanto desea que se cotice',
              addonRight: {
                text: this.product.item_type,
              },
              description: 'Seleccione el número de unidades que desea ' + this.labelQuoteAction.toLowerCase(),
              required: true,
            },
          },
          {
            className: 'col-6',
            type: 'textarea',
            key: 'quote_notes',
            templateOptions: {
              placeholder: 'Su mensaje, pregunta o inquietud',
              maxLength: 2000,
              rows: 2,
              required: true,
              attributes: {
                class: 'form-control form-control-sm'
              },
            },
          },
        ]
      },
      {
        key: 'askRelated',
        type: 'checkbox',
        templateOptions: {
          label: 'Obtener ' + this.labelQuotePlural.toLowerCase() + ' de productos similares',
          description: 'Se generaran solicitudes adicionales a exhibidores con productos similares',
          required: false,
        },
      },
    ];
  }
}
