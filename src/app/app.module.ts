import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { QuotesModule } from './quotes/quotes.module';
import { QuotesService } from './quotes/quotes.service';
import { HttpClientModule } from '@angular/common/http';
import { RECAPTCHA_SETTINGS, RecaptchaModule, RecaptchaSettings } from 'ng-recaptcha';

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    NgbModule,
    QuotesModule,
    RecaptchaModule
  ],
  providers: [
    QuotesService,
    {
      provide: RECAPTCHA_SETTINGS,
      useValue: {siteKey: '6LcJUMISAAAAAOXE8johxFCohVN1rOUbBZw9833Z'} as RecaptchaSettings,
    },

  ],
  bootstrap: [
    AppComponent
  ]
})

export class AppModule {
}
