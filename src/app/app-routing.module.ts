import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { QuotesComponent } from './quotes/quotes.component';

const routes: Routes = [
  //PUBLIC
  // {
  //   path: 'quote',
  //   loadChildren: './quotes/quotes.module#QuotesModule'
  // },
  // {
  //   path: '**',
  //   redirectTo: 'quote'
  // }
  {path: 'quote/:id', component: QuotesComponent}
];


@NgModule({
  imports: [RouterModule.forRoot(
    routes,
    //{ enableTracing: true }
  )],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
